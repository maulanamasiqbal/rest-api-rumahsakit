package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pasien")
public class Pasien extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "pasienSeq", sequenceName = "pasien_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "pasienSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "nama_lengkap", unique = true, nullable = false, columnDefinition = "varchar(255)")
    private String nama_lengkap;

    @Column(name = "gender", nullable = false, columnDefinition = "varchar(10)")
    private String gender;

    @Column(name = "status",  nullable = false, columnDefinition = "varchar(50)")
    private String status;

    @Column(name = "address", nullable = false, columnDefinition = "varchar(255)")
    private String address;

    @Column(name = "email", unique = true, nullable = false, columnDefinition = "varchar(255)")
    private String email;

    @Column(name = "phone_number", unique = true, nullable = false, columnDefinition = "varchar(20)")
    private String phone_number;

    @Column(name = "is_cover_bpjs", nullable = false, columnDefinition = "boolean")
    private boolean is_cover_bpjs;

    public Pasien() {
        super();
    }

    public Pasien(String nama_lengkap, String gender, String status, String address, String email, String phone_number, boolean is_cover_bpjs) {
        this.nama_lengkap = nama_lengkap;
        this.gender = gender;
        this.status = status;
        this.address = address;
        this.email = email;
        this.phone_number = phone_number;
        this.is_cover_bpjs = is_cover_bpjs;
    }

    public long getId() {
        return id;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public boolean isIs_cover_bpjs() {
        return is_cover_bpjs;
    }

    public void setIs_cover_bpjs(boolean is_cover_bpjs) {
        this.is_cover_bpjs = is_cover_bpjs;
    }
}
