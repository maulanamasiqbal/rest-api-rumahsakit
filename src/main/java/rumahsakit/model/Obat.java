package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "obat")
public class Obat extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "obatSeq", sequenceName = "obat_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "obatSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "nama_obat",  nullable = false, columnDefinition = "varchar(255)")
    private String nama_obat;

    @Column(name = "produksi", nullable = false, columnDefinition = "varchar(50)")
    private String produksi;

    @Column(name = "obat_kategori",  nullable = false, columnDefinition = "varchar(10)")
    @Enumerated(EnumType.STRING)
    private ObatEnum obat_kategori;

    @Column(name = "deskripsi",  nullable = false, columnDefinition = "varchar(255)")
    private String deskripsi;

    public Obat() {
        super();
    }

    public Obat(String nama_obat, String produksi, ObatEnum obat_kategori, String deskripsi) {
        this.nama_obat = nama_obat;
        this.produksi = produksi;
        this.obat_kategori = obat_kategori;
        this.deskripsi = deskripsi;
    }

    public long getId() {
        return id;
    }

    public String getNama_obat() {
        return nama_obat;
    }

    public void setNama_obat(String nama_obat) {
        this.nama_obat = nama_obat;
    }

    public String getProduksi() {
        return produksi;
    }

    public void setProduksi(String produksi) {
        this.produksi = produksi;
    }

    public ObatEnum getObat_kategori() {
        return obat_kategori;
    }

    public void setObat_kategori(ObatEnum obat_kategori) {
        this.obat_kategori = obat_kategori;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
