package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "perawat")
public class Perawat extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "perawatSeq", sequenceName = "perawat_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "perawatSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "nama_lengkap", unique = true, nullable = false, columnDefinition = "varchar(255)")
    private String nama_lengkap;

    @Column(name = "gender",  nullable = false, columnDefinition = "varchar(10)")
    private String gender;

    @Column(name = "gaji", nullable = false, columnDefinition = "bigint")
    private long gaji;

    @Column(name = "email", unique = true, nullable = false, columnDefinition = "varchar(255)")
    private String email;

    @Column(name = "phone_number", unique = true, nullable = false, columnDefinition = "varchar(20)")
    private String phone_number;

    @Column(name = "status",  nullable = false, columnDefinition = "varchar(50)")
    private String status;

    public Perawat() {
        super();
    }

    public Perawat(String nama_lengkap, String gender, long gaji, String email, String phone_number, String status) {
        this.nama_lengkap = nama_lengkap;
        this.gender = gender;
        this.gaji = gaji;
        this.email = email;
        this.phone_number = phone_number;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getGaji() {
        return gaji;
    }

    public void setGaji(long gaji) {
        this.gaji = gaji;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
