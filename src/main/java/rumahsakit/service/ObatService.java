package rumahsakit.service;

import io.vertx.core.json.JsonObject;
import rumahsakit.model.Obat;
import rumahsakit.model.ObatEnum;
import rumahsakit.model.Pasien;
import rumahsakit.model.StaffEnum;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class ObatService {

    @Inject
    EntityManager em;

    @Transactional
    public Response add(JsonObject params){
        Obat obat = new Obat();
        obat.setNama_obat(params.getString("nama_obat"));
        obat.setProduksi(params.getString("produksi"));

        String kategori = params.getString("kategori");

        switch (kategori.toLowerCase()){
            case "syrup":
                obat.setObat_kategori(ObatEnum.Syrup);
                break;
            case "pil":
                obat.setObat_kategori(ObatEnum.Pil);
                break;
            case "tablet":
                obat.setObat_kategori(ObatEnum.Tablet);
                break;
            case "cair":
                obat.setObat_kategori(ObatEnum.Cair);
                break;
            case "other":
                obat.setObat_kategori(ObatEnum.Other);
                break;
        }
        obat.setProduksi(params.getString("status"));
        obat.setDeskripsi(params.getString("deskripsi"));

        obat.persist();
        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", obat);

        return Response.ok().entity(result).build();
    }

    public Response getALl(String nama_obat, String produksi, String obat_kategori, String deskripsi){
        //        List<Country> countries = Country.findAll().page(Page.of(0,10)).list();
//        String nama_lengkap = "dummy1";
//        Long id = 1L;
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT *");
        sb.append(" FROM rumahsakit.obat o ");
        sb.append(" WHERE TRUE ");

        if(nama_obat != null){
            sb.append(" AND o.nama_obat = :nama_obat ");
        }
        if (obat_kategori != null){
            sb.append(" AND o.obat_kategori = :obat_kategori ");
        }
        if (produksi != null){
            sb.append(" AND o.produksi = :produksi ");
        }
        if (deskripsi != null){
            sb.append(" AND o.deskripsi = :deskripsi ");
        }
        Query query = em.createNativeQuery(sb.toString(), Obat.class);

        if(nama_obat != null){
            query.setParameter("nama_obat", nama_obat);
        }
        if (obat_kategori != null){
            query.setParameter("obat_kategori", obat_kategori);
        }
        if (produksi !=  null){
            query.setParameter("produksi", produksi);
        }
        if (deskripsi !=  null){
            query.setParameter("deskripsi", deskripsi);
        }
        List<Obat> obats = query.getResultList();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", obats);

        return Response.ok().entity(result).build();
    }

    public Response listKategori(){
            List<String> kategori_obat = Stream.of(ObatEnum.values()).map(e -> e.name()).collect(Collectors.toList());

            JsonObject result = new JsonObject();
            result.put("status", "success");
            result.put("data", kategori_obat);

            return Response.ok().entity(result).build();
    }

    @Transactional
    public Response update(Long id, JsonObject params){
        Obat obat = Obat.findById(id);
        if (obat==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Obat not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        obat.setNama_obat(params.getString("nama_obat"));
        obat.setProduksi(params.getString("produksi"));

        String kategori = params.getString("kategori");

        switch (kategori.toLowerCase()){
            case "syrup":
                obat.setObat_kategori(ObatEnum.Syrup);
                break;
            case "pil":
                obat.setObat_kategori(ObatEnum.Pil);
                break;
            case "tablet":
                obat.setObat_kategori(ObatEnum.Tablet);
                break;
            case "cair":
                obat.setObat_kategori(ObatEnum.Cair);
                break;
            case "other":
                obat.setObat_kategori(ObatEnum.Other);
                break;
        }
        obat.setProduksi(params.getString("status"));
        obat.setDeskripsi(params.getString("deskripsi"));

        obat.persist();
        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", obat);

        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response delete(Long id){
        Obat obat = Obat.findById(id);
        if (obat==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Obat not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        obat.delete();
        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", obat);

        return Response.ok().entity(result).build();
    }
}
