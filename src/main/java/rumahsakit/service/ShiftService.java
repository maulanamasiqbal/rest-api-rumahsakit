package rumahsakit.service;

import io.quarkus.panache.common.Page;
import io.vertx.core.json.JsonObject;
import rumahsakit.model.DaftarShift;
import rumahsakit.model.DaftraRawatInap;
import rumahsakit.model.RuangInap;
import rumahsakit.utility.DateUntil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ApplicationScoped
public class ShiftService {

    @Inject
    EntityManager em;

    @Transactional
    public Response add(JsonObject params){
        Set<String> hari = new HashSet<>(params.getJsonArray("hari").getList());
        DaftarShift daftarShift = new DaftarShift();

        daftarShift.setKategori(params.getString("kategori"));
        daftarShift.setForeign_id(params.getLong("foreign_id"));

        LocalDateTime time = LocalDateTime.now();
        daftarShift.setStartTime(time);
        daftarShift.setEndTime(time.plusHours(10));

        daftarShift.setHari(hari);

        daftarShift.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", daftarShift);
        return Response.ok().entity(result).build();
    }

    public Response getALl() {
        List<DaftarShift> daftarShiftList = DaftarShift.findAll().list();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", daftarShiftList);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response update(Long id, JsonObject params) {
        Set<String> hari = new HashSet<>(params.getJsonArray("hari").getList());
        DaftarShift daftarShift = new DaftarShift();

        if (daftarShift == null) {
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Shift not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }

        daftarShift.setKategori(params.getString("kategori"));
        daftarShift.setForeign_id(params.getLong("foreign_id"));

        LocalDateTime time = LocalDateTime.now();
        daftarShift.setStartTime(time);
        daftarShift.setEndTime(time.plusHours(8));

        daftarShift.setHari(hari);

        daftarShift.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", daftarShift);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response delete(Long id) {
        DaftarShift daftarShift = new DaftarShift();

        if (daftarShift == null) {
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Shift not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }

        daftarShift.delete();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", daftarShift);
        return Response.ok().entity(result).build();
    }
}
