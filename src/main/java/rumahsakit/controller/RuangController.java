package rumahsakit.controller;

import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import rumahsakit.service.DaftarService;
import rumahsakit.service.RuangService;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/rumahsakit/daftarRuangInap")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RuangController {

        @Inject
        RuangService ruangService;

        @POST
        @Operation(summary = "Add RuangInap")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @RolesAllowed({"superadmin", "user"})
        public Response add(JsonObject params){
            return ruangService.add(params);
        }

        @GET
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @PermitAll
        public Response getAll()
        {
            return ruangService.getALl();
        }

        @GET
        @Path("/kategori")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON)) @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        public Response getPoisi(){
                return ruangService.listKategori();
        }

        @PUT
        @Path("/{id}")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @RolesAllowed({"superadmin", "user"})
        public Response updateRuang(@Parameter(name = "id") @PathParam("id") Long id, JsonObject params){
            return ruangService.update(id, params);
        }

        @DELETE
        @Path("/{id}")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @RolesAllowed({"superadmin", "user"})
        public Response delete(@Parameter(name = "id") @PathParam("id") Long id){
            return ruangService.delete(id);
        }
}
