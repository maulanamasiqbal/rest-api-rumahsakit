package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "daftar_rawat_inap")
public class DaftraRawatInap extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "rawatSeq", sequenceName = "rawat_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "rawatSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @ManyToOne(targetEntity = Pasien.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "pasien_id", nullable = false)
    Pasien pasien;

    @ManyToOne(targetEntity = RuangInap.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ruang_id", nullable = false)
    RuangInap ruangInap;

    @ManyToOne(targetEntity = Dokter.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "dokter_id", nullable = false)
    Dokter dokter;

    @ManyToOne(targetEntity = Perawat.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "perawat_satu_id", nullable = false)
    Perawat perawatSatu;

    @ManyToOne(targetEntity = Perawat.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "perawat_dua_id", nullable = false)
    Perawat perawatDua;

    @Column(name = "start_time", nullable = false, columnDefinition = "timestamp")
    LocalDateTime startTime;

    @Column(name = "end_time", nullable = false, columnDefinition = "timestamp")
    LocalDateTime endTime;

    @Column(name = "is_checkout", nullable = false, columnDefinition = "boolean")
    private boolean is_checkout;

    public DaftraRawatInap() {
        super();
    }

    public DaftraRawatInap(Pasien pasien, RuangInap ruangInap, Dokter dokter, Perawat perawatSatu, Perawat perawatDua, LocalDateTime startTime, LocalDateTime endTime, boolean is_checkout) {
        this.pasien = pasien;
        this.ruangInap = ruangInap;
        this.dokter = dokter;
        this.perawatSatu = perawatSatu;
        this.perawatDua = perawatDua;
        this.startTime = startTime;
        this.endTime = endTime;
        this.is_checkout = is_checkout;
    }

    public long getId() {
        return id;
    }

    public Pasien getPasien() {
        return pasien;
    }

    public void setPasien(Pasien pasien) {
        this.pasien = pasien;
    }

    public RuangInap getRuangInap() {
        return ruangInap;
    }

    public void setRuangInap(RuangInap ruangInap) {
        this.ruangInap = ruangInap;
    }

    public Dokter getDokter() {
        return dokter;
    }

    public void setDokter(Dokter dokter) {
        this.dokter = dokter;
    }

    public Perawat getPerawatSatu() {
        return perawatSatu;
    }

    public void setPerawatSatu(Perawat perawatSatu) {
        this.perawatSatu = perawatSatu;
    }

    public Perawat getPerawatDua() {
        return perawatDua;
    }

    public void setPerawatDua(Perawat perawatDua) {
        this.perawatDua = perawatDua;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public boolean isIs_checkout() {
        return is_checkout;
    }

    public void setIs_checkout(boolean is_checkout) {
        this.is_checkout = is_checkout;
    }
}
