package rumahsakit.controller;

import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import rumahsakit.service.DaftarService;
import rumahsakit.service.JadwalService;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/rumahsakit/daftarRawatInap")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DaftarController {

        @Inject
        DaftarService daftarService;

        @POST
        @Operation(summary = "Add DaftarRuangInap")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @RolesAllowed({"superadmin", "user"})
        public Response add(JsonObject params){
            return daftarService.add(params);
        }

        @GET
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @PermitAll
        public Response getAll()
        {
            return daftarService.getALl();
        }

        @PUT
        @Path("/{id}")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @RolesAllowed({"superadmin", "user"})
        public Response updateDaftar(@Parameter(name = "id") @PathParam("id") Long id, JsonObject params){
            return daftarService.update(id, params);
        }

        @DELETE
        @Path("/{id}")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @RolesAllowed("superadmin")
        public Response delete(@Parameter(name = "id") @PathParam("id") Long id){
            return daftarService.delete(id);
        }
}
