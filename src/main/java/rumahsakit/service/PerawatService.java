package rumahsakit.service;

import io.vertx.core.json.JsonObject;
import rumahsakit.model.Dokter;
import rumahsakit.model.Perawat;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.util.List;

@ApplicationScoped
public class PerawatService {

    @Inject
    EntityManager em;

    @Transactional
    public Response add(JsonObject params){
        Perawat perawat = new Perawat();
        perawat.setNama_lengkap(params.getString("nama_lengkap"));
        perawat.setGender(params.getString("gender"));
        perawat.setGaji(params.getInteger("gaji"));
        perawat.setEmail(params.getString("email"));
        perawat.setPhone_number(params.getString("phone_number"));
        perawat.setStatus(params.getString("status"));

        perawat.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", perawat);

        return Response.ok().entity(result).build();
    }

    public Response getALl(String nama_lengkap, String email, String phone_number){
        //        List<Country> countries = Country.findAll().page(Page.of(0,10)).list();
//        String nama_lengkap = "dummy1";
//        Long id = 1L;
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT *");
        sb.append(" FROM rumahsakit.perawat p ");
        sb.append(" WHERE TRUE ");

        if(nama_lengkap != null){
            sb.append(" AND p.nama_lengkap = :nama_lengkap ");
        }
        if (email != null){
            sb.append(" AND p.email = :email ");
        }
        if (phone_number != null){
            sb.append(" AND p.phone_number = :phone_number ");
        }
        Query query = em.createNativeQuery(sb.toString(), Perawat.class);
        query.setFirstResult(0);
        query.setMaxResults(20);

        if(nama_lengkap != null){
            query.setParameter("nama_lengkap", nama_lengkap);
        }
        if (email != null){
            query.setParameter("email", email);
        }
        if (phone_number !=  null){
            query.setParameter("phone_number", phone_number);
        }
        List<Perawat> perawat = query.getResultList();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", perawat);

        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response update(Long id, JsonObject params){
        Perawat perawat = Perawat.findById(id);
        if (perawat==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Perawat not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        perawat.setNama_lengkap(params.getString("nama_lengkap"));
        perawat.setGender(params.getString("gender"));
        perawat.setGaji(params.getInteger("gaji"));
        perawat.setEmail(params.getString("email"));
        perawat.setPhone_number(params.getString("phone_number"));
        perawat.setStatus(params.getString("status"));

        perawat.persist();
        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", perawat);

        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response updateGaji(Long id, JsonObject params) {
        Perawat perawat = Perawat.findById(id);
        if (perawat == null) {
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Gaji Perawat not updated!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }

        perawat.setGaji(params.getLong("gaji"));
        perawat.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("message", "Gaji Perawat updated");
        result.put("data", perawat);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response delete(Long id){
        Perawat perawat = Perawat.findById(id);
        if (perawat==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Perawat not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        perawat.delete();
        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", perawat);

        return Response.ok().entity(result).build();
    }
}
