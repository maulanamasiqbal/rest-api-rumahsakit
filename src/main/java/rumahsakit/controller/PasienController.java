package rumahsakit.controller;

import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import rumahsakit.model.Pasien;
import rumahsakit.model.Perawat;
import rumahsakit.service.PasienService;
import rumahsakit.service.PerawatService;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/rumahsakit/pasien")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PasienController {

    @Inject
    PasienService pasienService;

    @POST
    @Operation(summary = "Add Pasien")
    @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
            @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
            @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
    })
    @RolesAllowed({"superadmin", "user"})
    public Response add(JsonObject params){
        return pasienService.add(params);
    }

    @GET
    @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
            @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON))
    })
    @PermitAll
    public Response getAll(
            @QueryParam("nama_lengkap") String nama_lengkap,
            @QueryParam("email") String email,
            @QueryParam("phone_number") String phone_number
    )
    {
        return pasienService.getALl(nama_lengkap, email, phone_number);
    }

    @PUT
    @Path("/{id}")
    @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
            @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
            @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
    })
    @RolesAllowed({"superadmin", "user"})
    public Response updatePasien(@Parameter(name = "id") @PathParam("id") Long id, JsonObject params){
        return pasienService.update(id, params);
    }

    @DELETE
    @Path("/{id}")
    @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
            @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
            @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
    })
    @RolesAllowed({"superadmin", "user"})
    public Response delete(@Parameter(name = "id") @PathParam("id") Long id){
        return pasienService.delete(id);
    }
}
