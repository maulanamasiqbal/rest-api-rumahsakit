package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "resep_obat")
public class ResepObat extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "resepSeq", sequenceName = "resep_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "reseoSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @ManyToOne(targetEntity = DaftarPertemuan.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "pertemuan_id", nullable = false)
    DaftarPertemuan pertemuan;

    @ManyToOne(targetEntity = Obat.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "obat_id", nullable = false)
    Obat obat;

    @Column(name = "dosis",  nullable = false, columnDefinition = "varchar(50)")
    private String dosis;

    @Column(name = "deskripsi",  nullable = false, columnDefinition = "varchar(255)")
    private String deskripsi;

    public ResepObat() {
        super();
    }

    public ResepObat(DaftarPertemuan pertemuan, Obat obat, String dosis, String deskripsi) {
        this.pertemuan = pertemuan;
        this.obat = obat;
        this.dosis = dosis;
        this.deskripsi = deskripsi;
    }

    public long getId() {
        return id;
    }

    public DaftarPertemuan getPertemuan() {
        return pertemuan;
    }

    public void setPertemuan(DaftarPertemuan pertemuan) {
        this.pertemuan = pertemuan;
    }

    public Obat getObat() {
        return obat;
    }

    public void setObat(Obat obat) {
        this.obat = obat;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
