package rumahsakit.service;

import io.quarkus.panache.common.Page;
import io.vertx.core.json.JsonObject;
import rumahsakit.model.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.List;

@ApplicationScoped
public class DaftarService {

    @Inject
    EntityManager em;

    @Transactional
    public Response add(JsonObject params) {
        DaftraRawatInap daftraRawatInap = new DaftraRawatInap();

        Pasien pasien = Pasien.findById(params.getLong("pasien_id", -99L));
        RuangInap ruangInap = RuangInap.findById(params.getLong("ruang_id", -99L));
        Dokter dokter = Dokter.findById(params.getLong("dokter_id", -99L));
        Perawat perawatSatu = Perawat.findById(params.getLong("perawat_satu_id", -99L));
        Perawat perawatDua = Perawat.findById(params.getLong("perawat_dua_id", -99L));

        if (pasien == null || ruangInap == null || dokter == null || perawatSatu == null || perawatDua == null) {
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Join not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }

        LocalDateTime time = LocalDateTime.now();
        daftraRawatInap.setStartTime(time);
        daftraRawatInap.setEndTime(time.plusHours(10));

        daftraRawatInap.setPasien(pasien);
        daftraRawatInap.setRuangInap(ruangInap);
        daftraRawatInap.setDokter(dokter);
        daftraRawatInap.setPerawatSatu(perawatSatu);
        daftraRawatInap.setPerawatDua(perawatDua);
        daftraRawatInap.persist();

        if (ruangInap.isIs_kosong() == false){
            ruangInap.setIs_kosong(true);
        } else if (daftraRawatInap.isIs_checkout() == false) {
            ruangInap.setIs_kosong(false);
        }

        ruangInap.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", daftraRawatInap);
        return Response.ok().entity(result).build();
    }

    public Response getALl() {
        List<DaftraRawatInap> daftraRawatInapList = RuangInap.findAll().page(Page.of(0, 10)).list();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", daftraRawatInapList);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response update(Long id, JsonObject params) {
        DaftraRawatInap daftraRawatInap = new DaftraRawatInap();

        if (daftraRawatInap == null) {
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Daftar not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }

        Pasien pasien = Pasien.findById(params.getLong("pasien_id", -99L));
        RuangInap ruangInap = RuangInap.findById(params.getLong("ruang_id", -99L));
        Dokter dokter = Dokter.findById(params.getLong("dokter_id", -99L));
        Perawat perawatSatu = Perawat.findById(params.getLong("perawat_satu_id", -99L));
        Perawat perawatDua = Perawat.findById(params.getLong("perawat_dua_id", -99L));

        if(pasien == null || ruangInap == null || dokter == null || perawatSatu == null || perawatDua == null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Join or id not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }

        LocalDateTime time = LocalDateTime.now();
        daftraRawatInap.setStartTime(time);
        daftraRawatInap.setEndTime(time.plusHours(8));

        daftraRawatInap.setIs_checkout(params.getBoolean("is_checkout"));
        daftraRawatInap.setPasien(pasien);
        daftraRawatInap.setRuangInap(ruangInap);
        daftraRawatInap.setDokter(dokter);
        daftraRawatInap.setPerawatSatu(perawatSatu);
        daftraRawatInap.setPerawatDua(perawatDua);
        daftraRawatInap.persist();

        if (daftraRawatInap.isIs_checkout() == true){
            ruangInap.setIs_kosong(true);
        } else if (daftraRawatInap.isIs_checkout() == false) {
            ruangInap.setIs_kosong(true);
        }
        ruangInap.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", daftraRawatInap);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response delete(Long id) {
        DaftraRawatInap daftraRawatInap = new DaftraRawatInap();

        if (daftraRawatInap == null) {
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Daftar not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }

        daftraRawatInap.delete();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", daftraRawatInap);
        return Response.ok().entity(result).build();
    }
}
