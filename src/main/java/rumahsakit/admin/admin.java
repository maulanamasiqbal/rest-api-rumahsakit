package rumahsakit.admin;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import org.jboss.logging.Logger;
import rumahsakit.model.User;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Stream;

@ApplicationScoped
public class admin {
    private static final Logger LOGGER = Logger.getLogger("ListenerBean");

    @Transactional
    void onStart(@Observes StartupEvent ev) {
        LOGGER.info("The application is starting...");


        List<User> users = User.findAll().list();

        Optional<User> getuser = users.stream().filter(u -> u.getEmail().equalsIgnoreCase("superadmin@gmail.com"))
                .findFirst();
        if (getuser.isEmpty()){
            String password = "super";
            User user = new User();
            user.setName("Super Admin");
            user.setUsername("superadmin");
            user.setEmail("superadmin@gmail.com");
            user.setUser_type("super admin");
            user.setPassword(Base64.getEncoder().encodeToString(password.getBytes(StandardCharsets.UTF_8)));
            user.setPhone("085156976647");

            Set<String> permissions = new HashSet<>();
            permissions.add("spesial");
            user.setPermission(permissions);
            user.persist();
        }

        Response.ok().build();
    }

    void onStop(@Observes ShutdownEvent ev) {
        LOGGER.info("The application is stopping...");
    }
}


