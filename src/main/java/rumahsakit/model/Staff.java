package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "staff")
public class Staff extends AuditModel implements Serializable {

    @Id
    @SequenceGenerator(name = "staffSeq", sequenceName = "staff_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "staffSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "nama_lengkap", unique = true, nullable = false, columnDefinition = "varchar(255)")
    private String nama_lengkap;

    @Column(name = "gender", nullable = false, columnDefinition = "varchar(10)")
    private String gender;

    @Column(name = "posisi", nullable = false, columnDefinition = "varchar(10)")
    @Enumerated(EnumType.STRING)
    private StaffEnum posisi;

    @Column(name = "start_time", nullable = true, columnDefinition = "timestamp")
    LocalDateTime startTime;

    @Column(name = "end_time", nullable = false, columnDefinition = "timestamp")
    LocalDateTime endTime;

    @Column(name = "gaji",  nullable = false, columnDefinition = "bigint")
    private long gaji;

    @Column(name = "email", unique = true, nullable = false, columnDefinition = "varchar(255)")
    private String email;

    @Column(name = "phone_number", unique = true, nullable = false, columnDefinition = "varchar(20)")
    private String phone_number;

    @Column(name = "status", nullable = false, columnDefinition = "varchar(50)")
    private String status;

    public Staff() {
        super();
    }

    public Staff(String nama_lengkap, String gender, StaffEnum posisi, LocalDateTime startTime, LocalDateTime endTime, long gaji, String email, String phone_number, String status) {
        this.nama_lengkap = nama_lengkap;
        this.gender = gender;
        this.posisi = posisi;
        this.startTime = startTime;
        this.endTime = endTime;
        this.gaji = gaji;
        this.email = email;
        this.phone_number = phone_number;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public StaffEnum getPosisi() {
        return posisi;
    }

    public void setPosisi(StaffEnum posisi) {
        this.posisi = posisi;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public long getGaji() {
        return gaji;
    }

    public void setGaji(long gaji) {
        this.gaji = gaji;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
