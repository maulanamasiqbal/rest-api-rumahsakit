package rumahsakit.model;

public enum ObatEnum {
    Syrup,
    Pil,
    Tablet,
    Cair,
    Other
}
