package rumahsakit.controller;

import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import rumahsakit.service.PerawatService;
import rumahsakit.service.StaffService;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/rumahsakit/staff")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StaffController {
        @Inject
        StaffService staffService;

        @POST
        @Operation(summary = "Add Staff")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @RolesAllowed({"superadmin", "user"})
        public Response add(JsonObject params){
            return staffService.add(params);
        }

        @GET
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @PermitAll
        public Response getAll(
                @QueryParam("nama_lengkap") String nama_lengkap,
                @QueryParam("email") String email,
                @QueryParam("phone_number") String phone_number
        )
        {
                return staffService.getALl(nama_lengkap, email, phone_number);
        }

        @GET
        @Path("/posisi")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON)) @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @PermitAll
        public Response getPoisi(){
                return staffService.listposisi();
        }

//        @PUT
//        @Path("/{id}")
//        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
//        @APIResponses(value = {
//                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
//                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
//                @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
//        })
//        @RolesAllowed("superadmin")
//        public Response updateStaff(@Parameter(name = "id") @PathParam("id") Long id, JsonObject params){
//            return staffService.update(id, params);
//        }

        @PUT
        @Path("/{id}")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @RolesAllowed("user")
        public Response updateGaji(@Parameter(name = "id") @PathParam("id") Long id, JsonObject params){
                return staffService.updateGaji(id, params);
        }

        @DELETE
        @Path("/{id}")
        @RequestBody(content = @Content(mediaType = MediaType.APPLICATION_JSON))
        @APIResponses(value = {
                @APIResponse(responseCode = "200", description = "OK", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "400", description = "BAD_REQUEST", content = @Content(mediaType = MediaType.APPLICATION_JSON)),
                @APIResponse(responseCode = "401", description = "Not Authorized", content = @Content(mediaType = MediaType.APPLICATION_JSON))
        })
        @RolesAllowed({"superadmin", "user"})
        public Response delete(@Parameter(name = "id") @PathParam("id") Long id){
            return staffService.delete(id);
        }

}
