package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@Entity
@Table(name = "riwayat_penyakit")
public class RiwayatPenyakit extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "riwayatSeq", sequenceName = "riwayat_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "riwayatSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @ManyToOne(targetEntity = Pasien.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "pasien_id", nullable = false)
    Pasien pasien;

    @Column(name = "nama", nullable = false, columnDefinition = "varchar(50)")
    private String nama;

    @Column(name = "deskripsi",  nullable = false, columnDefinition = "varchar(255)")
    private String deskripsi;

    @Column(name = "start_time", nullable = false, columnDefinition = "timestamp")
    LocalDateTime awalDate;

    @Column(name = "end_time", nullable = false, columnDefinition = "timestamp")
    LocalDateTime sembuhDate;

    public RiwayatPenyakit() {
        super();
    }

    public RiwayatPenyakit(Pasien pasien, String nama, String deskripsi, LocalDateTime awalDate, LocalDateTime sembuhDate) {
        this.pasien = pasien;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.awalDate = awalDate;
        this.sembuhDate = sembuhDate;
    }

    public long getId() {
        return id;
    }

    public Pasien getPasien() {
        return pasien;
    }

    public void setPasien(Pasien pasien) {
        this.pasien = pasien;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public LocalDateTime getAwalDate() {
        return awalDate;
    }

    public void setAwalDate(LocalDateTime awalDate) {
        this.awalDate = awalDate;
    }

    public LocalDateTime getSembuhDate() {
        return sembuhDate;
    }

    public void setSembuhDate(LocalDateTime sembuhDate) {
        this.sembuhDate = sembuhDate;
    }
}
