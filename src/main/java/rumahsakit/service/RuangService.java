package rumahsakit.service;

import io.quarkus.panache.common.Page;
import io.vertx.core.json.JsonObject;
import rumahsakit.model.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class RuangService {

    @Inject
    EntityManager em;

    @Transactional
    public Response add(JsonObject params){
        RuangInap ruangInap = new RuangInap();

        ruangInap.setPrefix_ruangan(params.getString("prefix_ruangan"));
        ruangInap.setNomor_ruangan(params.getString("nomor_ruangan"));

        String kategori_ruangan = params.getString("kategori_ruangan");

        switch (kategori_ruangan.toLowerCase()){
            case "standart":
                ruangInap.setKategori_ruangan(RuangKategori.Standard);
                break;
            case "vip":
                ruangInap.setKategori_ruangan(RuangKategori.VIP);
                break;
            case "vvip":
                ruangInap.setKategori_ruangan(RuangKategori.VVIP);
                break;
        }

        ruangInap.setIs_kosong(params.getBoolean("is_kosong"));

        ruangInap.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", ruangInap);

        return Response.ok().entity(result).build();
    }

    public Response getALl() {
        List<RuangInap> ruangInapList = RuangInap.findAll().page(Page.of(0, 10)).list();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", ruangInapList);
        return Response.ok().entity(result).build();
    }

    public Response listKategori(){
        List<String> kategori_ruangan = Stream.of(RuangKategori.values()).map(e -> e.name()).collect(Collectors.toList());

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", kategori_ruangan);

        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response update(Long id, JsonObject params) {
        RuangInap ruangInap = RuangInap.findById(id);
        if (ruangInap == null) {
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Ruang not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }

        if (ruangInap.isIs_kosong() == false){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Ruang sedang digunakan!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        ruangInap.setPrefix_ruangan(params.getString("prefix_ruangan"));
        ruangInap.setNomor_ruangan(params.getString("nomor_ruangan"));
        String kategori_ruangan = params.getString("kategori_ruangan");

        switch (kategori_ruangan.toLowerCase()) {
            case "standart":
                ruangInap.setKategori_ruangan(RuangKategori.Standard);
                break;
            case "vip":
                ruangInap.setKategori_ruangan(RuangKategori.VIP);
                break;
            case "vvip":
                ruangInap.setKategori_ruangan(RuangKategori.VVIP);
                break;
        }

        ruangInap.setIs_kosong(params.getBoolean("is_kosong"));

        ruangInap.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", ruangInap);

        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response delete(Long id){
        RuangInap ruangInap = RuangInap.findById(id);
        if (ruangInap==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Ruang not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        if (ruangInap.isIs_kosong() == false){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Ruang sedang di Gunakan!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        ruangInap.delete();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", ruangInap);
        return Response.ok().entity(result).build();
    }
}
