package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "daftar_shift")
public class DaftarShift extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "shiftSeq", sequenceName = "shift_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "shiftSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "kategori", nullable = false, columnDefinition = "varchar(50)")
    private String kategori;

    @Column(name = "foreign_id", nullable = false, columnDefinition = "bigInt")
    private long foreign_id;

    @Column(name = "start_time", nullable = false, columnDefinition = "timestamp")
    LocalDateTime startTime;

    @Column(name = "end_time", nullable = false, columnDefinition = "timestamp")
    LocalDateTime endTime;

    @ElementCollection
    @CollectionTable(name = "daftar_shift_hari", joinColumns = @JoinColumn(name = "hari_id"))
    @Column(name = "hari", nullable = false, columnDefinition = "varchar(50)")
    Set<String> hari = new HashSet<>();

    public DaftarShift() {
        super();
    }

    public DaftarShift(String kategori, long foreign_id, LocalDateTime startTime, LocalDateTime endTime, Set<String> hari) {
        this.kategori = kategori;
        this.foreign_id = foreign_id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.hari = hari;
    }

    public long getId() {
        return id;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public long getForeign_id() {
        return foreign_id;
    }

    public void setForeign_id(long foreign_id) {
        this.foreign_id = foreign_id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Set<String> getHari() {
        return hari;
    }

    public void setHari(Set<String> hari) {
        this.hari = hari;
    }
}
