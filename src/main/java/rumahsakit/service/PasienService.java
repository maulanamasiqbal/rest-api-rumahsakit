package rumahsakit.service;

import io.vertx.core.json.JsonObject;
import rumahsakit.model.Pasien;
import rumahsakit.model.Perawat;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.util.List;

@ApplicationScoped
public class PasienService {
    @Inject
    EntityManager em;

    @Transactional
    public Response add(JsonObject params){
        Pasien pasien = new Pasien();
        pasien.setNama_lengkap(params.getString("nama_lengkap"));
        pasien.setGender(params.getString("gender"));
        pasien.setStatus(params.getString("status"));
        pasien.setAddress(params.getString("address"));
        pasien.setEmail(params.getString("email"));
        pasien.setPhone_number(params.getString("phone_number"));
        pasien.setIs_cover_bpjs(params.getBoolean("is_cover_bpjs"));

        pasien.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", pasien);

        return Response.ok().entity(result).build();
    }

    public Response getALl(String nama_lengkap, String email, String phone_number){
        //        List<Country> countries = Country.findAll().page(Page.of(0,10)).list();
//        String nama_lengkap = "dummy1";
//        Long id = 1L;
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT *");
        sb.append(" FROM rumahsakit.pasien p ");
        sb.append(" WHERE TRUE ");

        if(nama_lengkap != null){
            sb.append(" AND p.nama_lengkap = :nama_lengkap ");
        }
        if (email != null){
            sb.append(" AND p.email = :email ");
        }
        if (phone_number != null){
            sb.append(" AND p.phone_number = :phone_number ");
        }
        Query query = em.createNativeQuery(sb.toString(), Pasien.class);
        query.setFirstResult(0);
        query.setMaxResults(20);

        if(nama_lengkap != null){
            query.setParameter("nama_lengkap", nama_lengkap);
        }
        if (email != null){
            query.setParameter("email", email);
        }
        if (phone_number !=  null){
            query.setParameter("phone_number", phone_number);
        }
        List<Pasien> pasiens = query.getResultList();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", pasiens);

        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response update(Long id, JsonObject params){
        Pasien pasien = Pasien.findById(id);
        if (pasien==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Pasien not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        pasien.setNama_lengkap(params.getString("nama_lengkap"));
        pasien.setGender(params.getString("gender"));
        pasien.setStatus(params.getString("status"));
        pasien.setAddress(params.getString("address"));
        pasien.setEmail(params.getString("email"));
        pasien.setPhone_number(params.getString("phone_number"));
        pasien.setIs_cover_bpjs(params.getBoolean("is_cover_bpjs"));

        pasien.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", pasien);

        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response delete(Long id){
        Pasien pasien = Pasien.findById(id);
        if (pasien==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Pasien not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        pasien.delete();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", pasien);

        return Response.ok().entity(result).build();
    }
}
