package rumahsakit.service;

import io.quarkus.panache.common.Page;
import io.vertx.core.json.JsonObject;
import rumahsakit.model.Dokter;
import rumahsakit.model.JadwalPraktik;
import rumahsakit.model.Pasien;
import rumahsakit.utility.DateUntil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.List;

@ApplicationScoped
public class JadwalService {
    @Inject
    EntityManager em;

    @Transactional
    public Response add(JsonObject params){
        JadwalPraktik jd = new JadwalPraktik();
        Dokter dokter = Dokter.findById(params.getLong("dokterId", -99L));
        jd.setHari(params.getString("hari"));

        LocalDateTime time = LocalDateTime.now();
        jd.setStartTime(time);
        jd.setEndTime(time.plusHours(10));

        jd.setDeskripsi(params.getString("deskripsi"));
        jd.setDokter(dokter);

        jd.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", jd);
        return Response.ok().entity(result).build();
    }

    public Response getALl(){
        List<JadwalPraktik> jadwalPraktikList = JadwalPraktik.findAll().list();
//
        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", jadwalPraktikList);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response update(Long id, JsonObject params){
        JadwalPraktik jd = JadwalPraktik.findById(id);
        if (jd==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Jadwal not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        Dokter dokter = Dokter.findById(params.getLong("dokterId", -99L));
        if(dokter == null ){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "dokter not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        jd.setHari(params.getString("hari"));

        LocalDateTime time = LocalDateTime.now();
        jd.setStartTime(time);
        jd.setEndTime(time.plusHours(8));

        jd.setDeskripsi(params.getString("deskripsi"));

        jd.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", jd);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response delete(Long id){
        JadwalPraktik jd = JadwalPraktik.findById(id);
        if (jd==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Jadwal not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        jd.delete();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", jd);
        return Response.ok().entity(result).build();
    }
}
