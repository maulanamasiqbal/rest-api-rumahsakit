package rumahsakit.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import java.time.LocalDateTime;

@MappedSuperclass
public class AuditModel extends PanacheEntityBase {

    @CreationTimestamp
    @Column(name = "crated_date_time")
    private LocalDateTime createdDateTime;

    @CreationTimestamp
    @Column(name = "updated_date_time")
    private LocalDateTime updatedDateTime;

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public LocalDateTime getUpdatedDateTime() {
        return updatedDateTime;
    }
}
