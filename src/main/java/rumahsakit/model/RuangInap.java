package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "ruang_inap")
public class RuangInap extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "ruangSeq", sequenceName = "ruang_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "ruangSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "prefx_ruangan",  nullable = false, columnDefinition = "varchar(250)")
    private String prefix_ruangan;

    @Column(name = "nomor_ruangan", nullable = false, columnDefinition = "varchar(250)")
    private String nomor_ruangan;

    @Column(name = "kategori_ruangan", nullable = false, columnDefinition = "varchar(50)")
    @Enumerated(EnumType.STRING)
    RuangKategori kategori_ruangan;

    @Column(name = "is_kosong", nullable = false, columnDefinition = "boolean")
    private boolean is_kosong;

    public RuangInap() {
        super();
    }

    public RuangInap(String prefix_ruangan, String nomor_ruangan, RuangKategori kategori_ruangan, boolean is_kosong) {
        this.prefix_ruangan = prefix_ruangan;
        this.nomor_ruangan = nomor_ruangan;
        this.kategori_ruangan = kategori_ruangan;
        this.is_kosong = is_kosong;
    }

    public long getId() {
        return id;
    }

    public String getPrefix_ruangan() {
        return prefix_ruangan;
    }

    public void setPrefix_ruangan(String prefix_ruangan) {
        this.prefix_ruangan = prefix_ruangan;
    }

    public String getNomor_ruangan() {
        return nomor_ruangan;
    }

    public void setNomor_ruangan(String nomor_ruangan) {
        this.nomor_ruangan = nomor_ruangan;
    }

    public RuangKategori getKategori_ruangan() {
        return kategori_ruangan;
    }

    public void setKategori_ruangan(RuangKategori kategori_ruangan) {
        this.kategori_ruangan = kategori_ruangan;
    }

    public boolean isIs_kosong() {
        return is_kosong;
    }

    public void setIs_kosong(boolean is_kosong) {
        this.is_kosong = is_kosong;
    }
}
