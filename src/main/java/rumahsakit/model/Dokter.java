package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dokter")
public class Dokter extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "dokterSeq", sequenceName = "dokter_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "dokterSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "nama_lengkap", unique = true, nullable = false, columnDefinition = "varchar(255)")
    private String nama_lengkap;

    @Column(name = "is_spesialis", nullable = false, columnDefinition = "boolean")
    private boolean is_spesialis;

    @Column(name = "spesialis_nama", columnDefinition = "varchar(255)")
    private String spesialis_nama;

    @Column(name = "email", unique = true, nullable = false, columnDefinition = "varchar(255)")
    private String email;

    @Column(name = "phone_number", unique = true, nullable = false, columnDefinition = "varchar(20)")
    private String phone_number;

    @Column(name = "status", nullable = false, columnDefinition = "varchar(50)")
    private String status;

    @Column(name = "gaji", nullable = false, columnDefinition = "bigint")
    private long gaji;

    public Dokter() {
        super();
    }

    public Dokter(String nama_lengkap, boolean is_spesialis, String spesialis_nama, String email, String phone_number, String status, long gaji) {
        this.nama_lengkap = nama_lengkap;
        this.is_spesialis = is_spesialis;
        this.spesialis_nama = spesialis_nama;
        this.email = email;
        this.phone_number = phone_number;
        this.status = status;
        this.gaji = gaji;
    }

    public long getId() {
        return id;
    }

    public boolean isIs_spesialis() {
        return is_spesialis;
    }

    public void setIs_spesialis(boolean is_spesialis) {
        this.is_spesialis = is_spesialis;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getSpesialis_nama() {
        return spesialis_nama;
    }

    public void setSpesialis_nama(String spesialis_nama) {
        this.spesialis_nama = spesialis_nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getGaji() {
        return gaji;
    }

    public void setGaji(long gaji) {
        this.gaji = gaji;
    }
}
