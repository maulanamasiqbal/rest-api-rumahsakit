package rumahsakit.service;

import io.vertx.core.json.JsonObject;
import rumahsakit.model.Dokter;
import rumahsakit.model.Perawat;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.util.List;

@ApplicationScoped
public class DokterService {

    @Inject
    EntityManager em;

    @Transactional
    public Response add(JsonObject params){
        Dokter dokter = new Dokter();
        dokter.setNama_lengkap(params.getString("nama_lengkap"));
        dokter.setIs_spesialis(params.getBoolean("is_spesialis"));
        dokter.setSpesialis_nama(params.getString("spesialis"));
        dokter.setGaji(params.getInteger("gaji"));
        dokter.setEmail(params.getString("email"));
        dokter.setPhone_number(params.getString("phone_number"));
        dokter.setStatus(params.getString("status"));

        dokter.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", dokter);
        return Response.ok().entity(result).build();
    }

    public Response getALl(String spesialis_nama, String nama_lengkap, String email, String phone_number ){
        //        List<Country> countries = Country.findAll().page(Page.of(0,10)).list();
//        String nama_lengkap = "dummy1";
//        Long id = 1L;
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT *");
        sb.append(" FROM rumahsakit.dokter d ");
        sb.append(" WHERE TRUE ");

        if(spesialis_nama != null){
            sb.append(" AND d.nama_lengkap = :nama_lengkap ");
        }
        if(nama_lengkap != null){
            sb.append(" AND d.nama_lengkap = :nama_lengkap ");
        }
        if (email != null){
            sb.append(" AND d.email = :email ");
        }
        if (phone_number != null){
            sb.append(" AND d.phone_number = :phone_number ");
        }
        Query query = em.createNativeQuery(sb.toString(), Dokter.class);
        query.setFirstResult(0);
        query.setMaxResults(20);

        if(spesialis_nama != null){
            query.setParameter("spesialis_nama", spesialis_nama);
        }
        if(nama_lengkap != null){
            query.setParameter("nama_lengkap", nama_lengkap);
        }
        if (email != null){
            query.setParameter("email", email);
        }
        if (phone_number !=  null){
            query.setParameter("phone_number", phone_number);
        }
        List<Dokter> dokters = query.getResultList();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", dokters);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response update(Long id, JsonObject params){
        Dokter dokter = Dokter.findById(id);
        if (dokter==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Dokter not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        dokter.setNama_lengkap(params.getString("nama_lengkap"));
        dokter.setIs_spesialis(params.getBoolean("is_spesialis"));
        dokter.setSpesialis_nama(params.getString("spesialis"));
        dokter.setGaji(params.getInteger("gaji"));
        dokter.setEmail(params.getString("email"));
        dokter.setPhone_number(params.getString("phone_number"));
        dokter.setStatus(params.getString("status"));

        dokter.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", dokter);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response updateGaji(Long id, JsonObject params) {
        Dokter dokter = Dokter.findById(id);
        if (dokter == null) {
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Dokter not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }

        dokter.setGaji(params.getLong("gaji"));
        dokter.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("message", "Gaji Dokter updated");
        result.put("data", dokter);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response delete(Long id){
        Dokter dokter = Dokter.findById(id);
        if (dokter==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Dokter not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        dokter.delete();
        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", dokter);
        return Response.ok().entity(result).build();
    }
}
