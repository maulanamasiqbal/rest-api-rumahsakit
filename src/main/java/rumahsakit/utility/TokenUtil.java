package rumahsakit.utility;

import io.smallrye.jwt.build.Jwt;
import rumahsakit.model.User;

public class TokenUtil {

    public static String generate(User user){
        return Jwt.issuer("http://rumahsakit/issuer")
                .expiresIn(600L)
                .upn(user.getUsername())
                .upn(user.getPassword())
                .groups(user.getPermission())
                .claim("email", user.getEmail())
                .sign();
    }
}
