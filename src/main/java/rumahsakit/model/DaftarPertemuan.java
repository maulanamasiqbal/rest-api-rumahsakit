package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "daftar_pertemuan")
public class DaftarPertemuan extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "pertemuanSeq", sequenceName = "pertemuan_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "pertemuanSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @ManyToOne(targetEntity = Pasien.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "pasien_id", nullable = false)
    Pasien pasien;

    @ManyToOne(targetEntity = Dokter.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "dokter_id", nullable = false)
    Dokter dokter;

    @Column(name = "kategori", nullable = false, columnDefinition = "varchar(50)")
    private String kategori;

    @Column(name = "deskripsi", nullable = false, columnDefinition = "varchar(255)")
    private String deskripsi;

    @Column(name = "tanggal", nullable = false, columnDefinition = "timestamp")
    LocalDateTime tanggal;

    public DaftarPertemuan() {
        super();
    }

    public DaftarPertemuan(Pasien pasien, Dokter dokter, String kategori, String deskripsi, LocalDateTime tanggal) {
        this.pasien = pasien;
        this.dokter = dokter;
        this.kategori = kategori;
        this.deskripsi = deskripsi;
        this.tanggal = tanggal;
    }

    public long getId() {
        return id;
    }

    public Pasien getPasien() {
        return pasien;
    }

    public void setPasien(Pasien pasien) {
        this.pasien = pasien;
    }

    public Dokter getDokter() {
        return dokter;
    }

    public void setDokter(Dokter dokter) {
        this.dokter = dokter;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public LocalDateTime getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDateTime tanggal) {
        this.tanggal = tanggal;
    }
}
