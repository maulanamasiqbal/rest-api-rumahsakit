package rumahsakit.service;

import io.vertx.core.json.JsonObject;
import rumahsakit.model.*;
import rumahsakit.utility.DateUntil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class StaffService {

    @Inject
    EntityManager em;

    @Transactional
    public Response add(JsonObject params){
        Staff staff =new Staff();

        staff.setNama_lengkap(params.getString("nama_lengkap"));
        staff.setGender(params.getString("gender"));

        String posisi = params.getString("kategori_posisi");

        switch (posisi.toLowerCase()){
            case "security":
                staff.setPosisi(StaffEnum.Security);
                break;
            case "janitor":
                staff.setPosisi(StaffEnum.Janitor);
                break;
            case "receipt":
                staff.setPosisi(StaffEnum.Receipt);
                break;
            case "engineer":
                staff.setPosisi(StaffEnum.Engineer);
                break;
        }

        LocalDateTime time = LocalDateTime.now();
        staff.setStartTime(time);
        staff.setEndTime(time.plusHours(10));
        staff.setGaji(params.getLong("gaji"));
        staff.setEmail(params.getString("email"));
        staff.setPhone_number(params.getString("phone_number"));
        staff.setStatus(params.getString("status"));

        staff.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", staff);

        return Response.ok().entity(result).build();
    }

    public Response getALl(String nama_lengkap, String email, String phone_number){
        //        List<Country> countries = Country.findAll().page(Page.of(0,10)).list();
//        String nama_lengkap = "dummy1";
//        Long id = 1L;
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT *");
        sb.append(" FROM rumahsakit.staff s ");
        sb.append(" WHERE TRUE ");

        if(nama_lengkap != null){
            sb.append(" AND s.nama_lengkap = :nama_lengkap ");
        }
        if (email != null){
            sb.append(" AND s.email = :email ");
        }
        if (phone_number != null){
            sb.append(" AND s.phone_number = :phone_number ");
        }
        Query query = em.createNativeQuery(sb.toString(), Staff.class);
        query.setFirstResult(0);
        query.setMaxResults(20);

        if(nama_lengkap != null){
            query.setParameter("nama_lengkap", nama_lengkap);
        }
        if (email != null){
            query.setParameter("email", email);
        }
        if (phone_number !=  null){
            query.setParameter("phone_number", phone_number);
        }
        List<Staff> staff = query.getResultList();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", staff);

        return Response.ok().entity(result).build();
    }

    public Response listposisi(){
        List<String> posisi = Stream.of(StaffEnum.values()).map(e -> e.name()).collect(Collectors.toList());

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", posisi);

        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response update(Long id, JsonObject params){
        Staff staff = Staff.findById(id);
        if (staff==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Staff not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        staff.setNama_lengkap(params.getString("nama_lengkap"));
        staff.setGender(params.getString("gender"));

        String posisi = params.getString("kategori_posisi");

        switch (posisi.toLowerCase()){
            case "security":
                staff.setPosisi(StaffEnum.Security);
                break;
            case "janitor":
                staff.setPosisi(StaffEnum.Janitor);
                break;
            case "receipt":
                staff.setPosisi(StaffEnum.Receipt);
                break;
            case "engineer":
                staff.setPosisi(StaffEnum.Engineer);
                break;
        }
        LocalDateTime time = LocalDateTime.now();
        staff.setStartTime(time);
        staff.setEndTime(time.plusHours(9));
        staff.setGaji(params.getLong("gaji"));
        staff.setEmail(params.getString("email"));
        staff.setPhone_number(params.getString("phone_number"));
        staff.setStatus(params.getString("status"));

        staff.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", staff);

        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response updateGaji(Long id, JsonObject params) {
        Staff staff = Staff.findById(id);
        if (staff == null) {
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Staff not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }

        staff.setGaji(params.getLong("gaji"));
        staff.persist();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("message", "Gaji Staff updated");
        result.put("data", staff);
        return Response.ok().entity(result).build();
    }

    @Transactional
    public Response delete(Long id){
        Staff staff = Staff.findById(id);
        if (staff==null){
            JsonObject result = new JsonObject();
            result.put("status", "error");
            result.put("message", "Staff not found!!");
            return Response.status(Response.Status.BAD_REQUEST).entity(result).build();
        }
        staff.delete();

        JsonObject result = new JsonObject();
        result.put("status", "success");
        result.put("data", staff);
        return Response.ok().entity(result).build();
    }
}
