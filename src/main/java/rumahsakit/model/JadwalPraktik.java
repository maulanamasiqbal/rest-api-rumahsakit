package rumahsakit.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "jadwal_praktik")
public class JadwalPraktik extends AuditModel implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "jadwalSeq", sequenceName = "jadwal_sequence", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "jadwalSeq", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "hari", nullable = false, columnDefinition = "varchar(50)")
    private String hari;

    @Column(name = "start_time", nullable = false, columnDefinition = "timestamp")
    LocalDateTime startTime;

    @Column(name = "end_time", nullable = false, columnDefinition = "timestamp")
    LocalDateTime endTime;

    @Column(name = "deskripsi", nullable = false, columnDefinition = "varchar(255)")
    private String deskripsi;

    @ManyToOne(targetEntity = Dokter.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "dokter_id", nullable = false)
    Dokter dokter;

    public JadwalPraktik() {
        super();
    }

    public JadwalPraktik(String hari, LocalDateTime startTime, LocalDateTime endTime, String deskripsi, Dokter dokter) {
        this.hari = hari;
        this.startTime = startTime;
        this.endTime = endTime;
        this.deskripsi = deskripsi;
        this.dokter = dokter;
    }

    public long getId() {
        return id;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Dokter getDokter() {
        return dokter;
    }

    public void setDokter(Dokter dokter) {
        this.dokter = dokter;
    }
}
