package rumahsakit.utility;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BasicUtil {
    public static List<Map<String, Object>> createListOfMapFromArray(List<Object[]> list,
                                                                     String... columnNames) throws ValidationException {

        if (list == null) {
            return new ArrayList<>();
        }

        if (list.size() > 0) {
            if (list.get(0).length > columnNames.length) {
                throw new ValidationException("Invalid Argument");
            }
        }

        List<Map<String, Object>> result = new ArrayList<>();
        for (Object[] item : list) {
            Map<String, Object> temp = new HashMap<>();
            for (int i = 0; i < columnNames.length; i++) {
                try {
                        temp.put(columnNames[i], item[i]);
                } catch (ArrayIndexOutOfBoundsException ex) {
                }
            }
            result.add(temp);
        }

        return result;
    }
}
